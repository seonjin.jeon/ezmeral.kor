import os
import re
import time
from typing import Any, Callable, Dict, List, Optional, Tuple, Type, Union

import matplotlib.pyplot as plt
import PIL
import requests
from openai import OpenAI
from PIL import Image
from google.colab.patches import cv2_imshow

from autogen import Agent, AssistantAgent, ConversableAgent, UserProxyAgent
from autogen.agentchat.contrib.img_utils import _to_pil, get_image_data
from autogen.agentchat.contrib.multimodal_conversable_agent import MultimodalConversableAgent


llm_config = {
    "config_list": config_list,
    "seed": 42,
    "temperature": 0,
}

def termination_msg(x):
   return isinstance(x,dict) and "TERMINATE" == str(x.get("content",""))[-9:].upper()

config_list_4o = autogen.config_list_from_json(
    "OAI_CONFIG_LIST.json",
    filter_dict={
        "model": ["gpt-4o"],
    },
)

config_list_dalle = autogen.config_list_from_json(
    "OAI_CONFIG_LIST.json",
    filter_dict={
        "model": ["dall_e-3"],
    },
)

def dalle_call(client, prompt, model="dall-e-3", size="1024x1024", quality="standard", n=1) -> str:
    response = client.images.generate(
        model=model,
        prompt=prompt,
        size=size,
        quality=quality,
        n=n,
    )
    image_url = response.data[0].url
    img_data = get_image_data(image_url)
    return image_url
  
class DALLEAgent(ConversableAgent):
    def __init__(self, name, llm_config=llm_config, **kwargs):
      super().__init__(name, llm_config=llm_config, **kwargs)

      try:
        config_list = llm_config["config_list"]
        api_key = config_list[0]["api_key"]
      except Exception as e:
        print("Unable to fetch API Key, because", e)
        api_key = config_list[0]["api_key"]
      self.client = OpenAI(api_key=api_key)
      self.register_reply([Agent, None], DALLEAgent.generate_dalle_reply)

    def generate_dalle_reply(self, messages, sender, config):
      client = self.client if config is None else config
      
      if client is None:
        return False, None
      if messages is None:
        messages = self._oai_messages[sender]
      
      prompt = messages[-1]["content"]
      img_data = dalle_call(client=self.client, prompt=prompt)
      #cv2_imshow(img_data)

      # plt.imshow(_to_pil(img_data))
      # plt.axis("off")
      # plt.show()
      return True, 'result.jpg'


painter = DALLEAgent(
    name="Painter",
    llm_config={"config_list": config_list_dalle},
)

user_proxy = UserProxyAgent(
    name = "User_proxy",
    system_message = "A human admin.",
    human_input_mode = "NEVER",
    max_consecutive_auto_reply= 0,
)

user_proxy.initiate_chat(
    painter,
    message="갈색의 털을 가진 귀여운 강아지를 그려줘",
)