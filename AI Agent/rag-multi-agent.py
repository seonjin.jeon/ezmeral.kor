# Multi-Agent with RAG
# userproxyAgent(사용자 역할) <-> AssistantAgent(LLM : user 요청을 처리하는 역할)
# RetrieveUserProxyAgent <-> RetrieveAssistantAgent
#!pip install autogen
#!pip install chromadb pypdf sentence_transformers 

import autogen
from autogen import AssistantAgent
from autogen.agentchat.contrib.retrieve_user_proxy_agent import RetrieveUserProxyAgent
from chromadb.utils import embedding_functions

openai_api_key = "sk-xxx"
config_list = autogen.config_list_from_json(
    "OAI_CONFIG_LIST.json",
    file_location=".",
    filter_dict={
        "model": ["gpt-4-turbo-preview"],
    },
)

llm_config = {
    "config_list": config_list,
    "seed": 42,
    "temperature": 0,
}

assistant = AssistantAgent(
    name="rag-assistant",
    system_message="You are a helpful assistant.",
    llm_config=llm_config,
)


rag_user_proxy = RetrieveUserProxyAgent(
    name="rag_user_proxy",
    retrieve_config={
        "task": "qa",
        "docs_path": "https://raw.githubusercontent.com/microsoft/autogen/main/README.md",
        "collection_name": "openai-embedding-3",
  	},
)

assistant.reset()
rag_user_proxy.initiate_chat(assistant, message="AutoGen이 뭐야?")
