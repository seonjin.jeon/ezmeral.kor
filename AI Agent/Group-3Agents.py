# Group Multi-Agent using RAG
#
# user : UserProxyAgent without RAG
# user_rag : RetrieveUserProxyAgent
# coder : Coder AssistantAgent
# pm : Product Manager AssistantAgent


import autogen
from autogen import AssistantAgent, UserProxyAgent
from autogen.agentchat.contrib.retrieve_user_proxy_agent import RetrieveUserProxyAgent
from chromadb.utils import embedding_functions

openai_api_key = "sk-xxx"

llm_config = {
    "config_list": [{
          "model": "gpt-4-turbo-preview",
          "api_key": openai_api_key
		  }],
    "seed": 42,
    "temperature": 0,
}

def termination_msg(x):
   return isinstance(x,dict) and "TERMINATE" == str(x.get("content",""))[-9:].upper()
   
user = UserProxyAgent(
    name="Admin",
    is_termination_msg=termination_msg,
    human_input_mode="NEVER",
    system_message="The boss who ask questions and give tasks.",
    code_execution_config=False,
    default_auto_reply="Reply `TERMINATE` if the task is done.",
)

user_rag = RetrieveUserProxyAgent(
    name="Admin_RAG",
    is_termination_msg=termination_msg,
    human_input_mode="NEVER",
    system_message="Assistant who has extra content retrieve power for solving difficult problems.",
    code_execution_config=False,
    max_consecutive_auto_reply=3,
    retrieve_config={
        "task": "code",
        "docs_path": "https://raw.githubusercontent.com/microsoft/autogen/main/samples/apps/autogen-studio/README.md",
        "chunk_token_size": 1000,
        "collection_name": "groupchat-rag",
  	},
)

coder = AssistantAgent(
    name="Senior_Python_Engineer",
    is_termination_msg=termination_msg,
    system_message="You are a senior python engineer. Reply `TERMINATE` in the end when everything is done.",
    llm_config=llm_config,
)


pm = AssistantAgent(
    name="Product_Manager",
    is_termination_msg=termination_msg,
    system_message="You are a product manager. Reply `TERMINATE` in the end when everything is done.",
    llm_config=llm_config,
)

PROBLEM = "Autogen Studio는 무엇이고 AutoGen Studio로 어떤 제품을 만들 수 있을까?"

def _reset_agents():
    user.reset()
    user_rag.reset()
    coder.reset()
    pm.reset()

def rag_chat():
    _reset_agents()
    groupchat = autogen.GroupChat(
        agents=[user_rag, coder, pm],
        messages=[],
        max_round=12,
        speaker_selection_method="auto",
        allow_repeat_speaker=False
        )
    manager = autogen.GroupChatManager(
         groupchat=groupchat,
         llm_config=llm_config
         )
    user_rag.initiate_chat(
        manager,
        message=PROBLEM,
        )

def norag_chat():
    _reset_agents()
    groupchat = autogen.GroupChat(
        agents=[user, coder, pm],
        messages=[],
        max_round=12,
        speaker_selection_method="auto",
        allow_repeat_speaker=False
        )
    manager = autogen.GroupChatManager(
         groupchat=groupchat,
         llm_config=llm_config
         )
    user.initiate_chat(
        manager,
        message=PROBLEM,
        )

norag_chat()

rag_chat()