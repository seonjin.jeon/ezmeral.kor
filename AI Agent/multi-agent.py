# Multi-Agent using autogen Framework
# userproxyAgent(사용자 역할) <-> AssistantAgent(LLM : user 요청을 처리하는 역할)

# pip install autogen

import json

openai_api_key = "sk-xxx"
with open('OAI_CONFIG_LIST.json', 'w') as f:
  config_list = [
      {
          "model": "gpt-4-turbo-preview",
          "api_key": openai_api_key
      },
      {
          "model": "gpt-4o",
          "api_key": openai_api_key
      },
      {
          "model": "dall_e-3",
          "api_key": openai_api_key
      },
  ]
  json.dump(config_list, f)

import autogen
config_list = autogen.config_list_from_json(
    "OAI_CONFIG_LIST.json",
    file_location=".",
    filter_dict={
        "model": ["gpt-4-turbo-preview"],
    },
)

llm_config = {
    "config_list": config_list,
    "seed": 42,
    "temperature": 0,
}

from autogen import AssistantAgent, UserProxyAgent

assistant = AssistantAgent(
    name="assistant",
    llm_config=llm_config,
)
user_proxy = UserProxyAgent(
    name="user_proxy",
    is_termination_msg=lambda x: x.get("content", "") and x.get("content","").rstrip().endswith("TERMINATE"),
    code_execution_config={"work_dir": "coding", "use_docker": False},
    human_input_mode="NEVER",
)

user_proxy.initiate_chat(assistant, message="""
삼성전자의 지난 3개월 주식 가격 그래프를 그려서 samsung_stock_price.png 파일로 저장해줘.
plotly 라이브러리를 사용하고 그래프 아래를 투명한 녹색으로 채워줘.
값을 잘 확인할 수 있도록 y축은 구간 최솟값에서 시작하도록 해줘.
이미지 비율은 보기 좋게 적절히 설정해줘.
""")

