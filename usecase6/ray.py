import ray
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.datasets import load_iris

# Ray 초기화
remote_conn = "ray://kuberay-head-svc.kuberay:10001"
ray.init(address=remote_conn)

# 분산 학습에 사용할 함수 정의
@ray.remote
def train_model(X_train, y_train, X_test, y_test):
    model = RandomForestClassifier(n_estimators=100)
    model.fit(X_train, y_train)
    predictions = model.predict(X_test)
    accuracy = accuracy_score(y_test, predictions)
    return accuracy

# 데이터 로드 및 분할
data = load_iris()
X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.2, random_state=42)

# 데이터를 분산하여 각 워커에 전달
X_train_ids = ray.put(X_train)
y_train_ids = ray.put(y_train)
X_test_ids = ray.put(X_test)
y_test_ids = ray.put(y_test)

# 병렬로 모델 학습 실행
result_ids = [train_model.remote(X_train_ids, y_train_ids, X_test_ids, y_test_ids) for _ in range(5)]

# 결과 수집
accuracies = ray.get(result_ids)

# 결과 출력
print("Accuracies:", accuracies)

# Ray 종료
ray.shutdown()
