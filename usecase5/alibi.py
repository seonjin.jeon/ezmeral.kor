!pip install alibi

import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from alibi.explainers import AnchorTabular

# Iris 데이터셋 로드
data = load_iris()
X = data.data
y = data.target

# 학습 데이터 및 테스트 데이터 분할
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# RandomForest 분류기 학습
clf = RandomForestClassifier(n_estimators=100, random_state=42)
clf.fit(X_train, y_train)


# Alibi-Explain을 사용하여 설명 생성
explainer = AnchorTabular(predictor=clf.predict, feature_names=data.feature_names)
explainer.fit(X_train)

# 테스트 샘플 선택
sample_idx = 0
X_instance = X_test[sample_idx].reshape(1, -1)

# 모델의 예측
prediction = clf.predict(X_instance)

# Alibi-Explain을 사용하여 설명 생성
explanation = explainer.explain(X_instance)

# 설명 출력
print('예측:', prediction)
print('설명:', explanation['anchor'])
print('확률:', float(explanation['precision']))

