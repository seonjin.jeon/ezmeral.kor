!pip install transformers==4.40.1 datasets==2.19.0 huggingface_hub==0.23.0 -qqq

# 1. 모델 학습에 사용할 연합뉴스 데이터셋 다운로드
from datasets import load_dataset
klue_tc_train = load_dataset('klue', 'ynat', split='train')
klue_tc_eval = load_dataset('klue', 'ynat', split='validation')

print(klue_tc_train[0])
print(klue_tc_train.features['label'].names)


# 2. 실습에 사용하지 않는 불필요한 컬럼 제거
klue_tc_train = klue_tc_train.remove_columns(['guid', 'url', 'date'])
klue_tc_eval = klue_tc_eval.remove_columns(['guid', 'url', 'date'])
klue_tc_train

# 3. 카테고리를 문자로 표기한 label_str 컬럼 추가
print(klue_tc_train.features['label'])
klue_tc_label = klue_tc_train.features['label']
def make_str_label(batch):
  batch['label_str'] = klue_tc_label.int2str(batch['label'])
  return batch

klue_tc_train = klue_tc_train.map(make_str_label, batched=True, batch_size=1000)

print(klue_tc_train[0])

# 4. 학습/검증/테스트 데이터셋 분할
train_dataset = klue_tc_train.train_test_split(test_size=10000, shuffle=True, seed=42)['test']
dataset = klue_tc_eval.train_test_split(test_size=1000, shuffle=True, seed=42)
test_dataset = dataset['test']
valid_dataset = dataset['train'].train_test_split(test_size=1000, shuffle=True, seed=42)['test']

# 5. Trainer를 사용한 학습: (1) 준비
import torch
import numpy as np
from transformers import (
    Trainer,
    TrainingArguments,
    AutoModelForSequenceClassification,
    AutoTokenizer
)

def tokenize_function(examples):
    return tokenizer(examples["title"], padding="max_length", truncation=True)

model_id = "klue/roberta-base"
model = AutoModelForSequenceClassification.from_pretrained(model_id, num_labels=len(train_dataset.features['label'].names))
tokenizer = AutoTokenizer.from_pretrained(model_id)

train_dataset = train_dataset.map(tokenize_function, batched=True)
valid_dataset = valid_dataset.map(tokenize_function, batched=True)
test_dataset = test_dataset.map(tokenize_function, batched=True)

# 6. Trainer를 사용한 학습: (2) 학습 인자와 평가 함수 정의
llm_save_path = 'llm-' + model_id.replace("/","-")
training_args = TrainingArguments(
    output_dir="./" + llm_save_path,
    num_train_epochs=1,
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    evaluation_strategy="epoch",
    learning_rate=5e-5,
    push_to_hub=False
)

def compute_metrics(eval_pred):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return {"accuracy": (predictions == labels).mean()}

# 7. Trainer를 사용한 학습 - (3) 학습 진행
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=valid_dataset,
    tokenizer=tokenizer,
    compute_metrics=compute_metrics,
)

trainer.train()

print(trainer.evaluate(test_dataset))

# 8. 모델의 예측 아이디와 문자열 레이블을 연결할 데이터를 모델 config에 저장
id2label = {i: label for i, label in enumerate(train_dataset.features['label'].names)}
label2id = {label: i for i, label in id2label.items()}
model.config.id2label = id2label
model.config.label2id = label2id

from huggingface_hub import login

login(token="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")
repo_id = f"jeonseonjin/{llm_save_path}"

# Trainer를 사용한 경우
trainer.push_to_hub(repo_id)

# 직접 학습한 경우
#model.push_to_hub(repo_id)
#tokenizer.push_to_hub(repo_id)

# 9. 모델 추론하기
from transformers import pipeline

model_id = f"jeonseonjin/{llm_save_path}"
model_pipeline = pipeline("text-classification", model=model_id)
model_pipeline(dataset['test']["title"][:5])