!pip install transformers==4.40.1 datasets==2.19.0 huggingface_hub==0.23.0 sentence_transformers -qqq

from sentence_transformers import SentenceTransformer, models
transformer_model = models.Transformer('BAAI/bge-m3')

pooling_layer = models.Pooling(
    transformer_model.get_word_embedding_dimension(),
    pooling_mode_mean_tokens=True,
    pooling_mode_cls_token=False,
    pooling_mode_max_tokens=False,
)
embedding_model = SentenceTransformer(modules=[transformer_model, pooling_layer])

from datasets import load_dataset
sts_train = load_dataset('klue','sts', split='train')
sts_test  = load_dataset('klue','sts', split='validation')
sts_train[0]

sts_train = sts_train.train_test_split(test_size=0.1,seed=42)
sts_train, sts_test = sts_train['train'], sts_train['test']

from sentence_transformers import InputExample

def prepare_sts_examples(dataset):
  examples = []
  for example in dataset:
    examples.append(InputExample(
        texts=[example['sentence1'], example['sentence2']],
        label=example['labels']['label'] / 5.0 ))
  return examples 
  
train_examples = prepare_sts_examples(sts_train)
test_examples = prepare_sts_examples(sts_test)

from torch.utils.data import DataLoader
train_dataloader = DataLoader(train_examples, shuffle=True, batch_size=16)

from sentence_transformers.evaluation import EmbeddingSimilarityEvaluator
test_evaluator = EmbeddingSimilarityEvaluator.from_input_examples(test_examples)
test_evaluator(embedding_model)

from sentence_transformers import losses

num_epochs = 1
model_name = "BAAI/bge-m3"
train_loss = losses.CosineSimilarityLoss(embedding_model)
model_save_path = './embedding-' + model_name.replace("/","-")

embedding_model.fit(
    train_objectives=[(train_dataloader, train_loss)],
    epochs=num_epochs,
    evaluator=test_evaluator,
    evaluation_steps=1000,
    warmup_steps=100,
    output_path=model_save_path )
	
trained_embedding_model = SentenceTransformer(model_save_path)
print(test_evaluator(trained_embedding_model))

from huggingface_hub import login

login(token="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")
repo_id = f"jeonseonjin/embedding_BAAI-bge-m3"
embedding_model.push_to_hub(repo_id)

# 9. 모델 추론하기
from sentence_transformers import SentenceTransformer
from datasets import load_dataset

model_id = f"jeonseonjin/embedding_BAAI-bge-m3"
sentence_model = SentenceTransformer(model_id)
klue_mrc_dataset = load_dataset('klue','mrc',split='train')

klue_mrc_dataset = klue_mrc_dataset.train_test_split(train_size=10, shuffle=False)['train']
embeddings = sentence_model.encode(klue_mrc_dataset['context'])
print(embeddings.shape)
print(embeddings[0])

