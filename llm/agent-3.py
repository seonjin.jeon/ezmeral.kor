import random
from langchain.agents import AgentType, initialize_agent, Tool
from langchain_openai import ChatOpenAI
from langchain.tools import WriteFileTool

chat = ChatOpenAI(
    temperature=0,
    # model="gpt-4o",
    # api_key="sk-xx",
    model="deepseek-ai/DeepSeek-Coder-V2-Lite-Instruct",
    base_url="http://34.64.183.14:8000/v1/"
    )

tools = []
tools.append(WriteFileTool(
    root_dir="./"
))

def min_limit_random_number(min_number):
    return random.randint(int(min_number), 100000)

tools.append(
    Tool(
        name="Random",
        description="특정 최솟값 이상의 임의의 숫자를 생성할 수 있습니다",
        func=min_limit_random_number
    )
)

agent = initialize_agent(
    tools,
    chat,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
    verbose=True
)

result = agent.run("10이상의 난수를 생성해 random.txt 파일에 저장하세요.")
print(f"Result: {result}")