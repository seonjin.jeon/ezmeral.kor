#pip install openai==0.28 
#wget https://raw.githubusercontent.com/wikibook/langchain/master/requirements.txt
#python3 -m pip install -r ./requirements.txt
#pip install langchain_community langchain-openai

from langchain.agents import AgentType, initialize_agent, Tool
from langchain.agents.agent_toolkits import create_retriever_tool

from langchain_openai import ChatOpenAI
from langchain_community.retrievers import WikipediaRetriever
from langchain.tools import WriteFileTool
import os
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

os.environ['CURL_CA_BUNDLE'] = ""
os.environ['PYTHONWARNINGS']="ignore:Unverified HTTPS request"

chat = ChatOpenAI(
    temperature=0,
    api_key="null",
    model="deepseek-ai/DeepSeek-Coder-V2-Lite-Instruct",
    base_url="http://34.64.183.14:8000/v1/"
    )

tools = []
tools.append(WriteFileTool(
    root_dir="./"
))

retriever = WikipediaRetriever(
    language="ko",
    doc_content_chars_max=500,
    top_k_results=1
)

tools.append(
    create_retriever_tool(
        name="WikipediaRetriever",
        description="받은 단어에 대한 위키백과 기사를 검색할 수 있다",
        retriever=retriever
    )
)

agent = initialize_agent(
    tools,
    chat,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
    verbose=True
)

result = agent.run("1부터 10까지를 합계하는 파이썬 코드를 위키백과 기사에서 검색해서 result.txt파일에 저장하세요.")
print(f"Result: {result}")