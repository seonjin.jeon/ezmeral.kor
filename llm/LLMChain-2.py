#pip install bs4

from langchain.chains import LLMChain, LLMRequestsChain
from langchain.prompts import PromptTemplate
from langchain_community.chat_models import ChatOpenAI


chat = ChatOpenAI(
    temperature=0,
    model="gpt-4o",
    api_key="sk-xxx",
)

prompt = PromptTemplate(
    input_variables=["query","requests_result"],
    template="""아래 문장을 바탕으로 질문에 답해 주세요.
    문장: {requests_result}
    질문: {query}""",
)

llmchain = LLMChain(
    llm=chat,
    prompt=prompt,
    verbose=True,
)

chain = LLMRequestsChain(
    llm_chain=llmchain,
)

print(chain({
    "query": "도교의 날씨를 알려주세요",
    "url": "https://www.jma.go.jp/bosai/forecast/data/overview_forecast/130000.json"
}))