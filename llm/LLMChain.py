from langchain import LLMChain, PromptTemplate
from langchain_community.chat_models import ChatOpenAI


chat = ChatOpenAI(
    temperature=0,
    model="gpt-4o",
    api_key="sk-xxxx"
)

prompt = PromptTemplate(
    template="{product}는 어느 회사에서 개발한 제품인가요?",
    input_variables=[
        "product"
    ]
)

chain = LLMChain(
    llm=chat,
    prompt=prompt,
)

result = chain.predict(product="iPhone")
print(result)