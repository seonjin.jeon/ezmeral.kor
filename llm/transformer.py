# Installation Huggingface Library
!pip install transformers==4.40 datasets==2.19.0 huggingface_hub==0.23.0 -qqq

# config.json : Overall Model Configuration

#
# 1. Dataset
#

from datasets import load_dataset, Dataset
klue_ynat_train = load_dataset('klue','ynat',split='train')
klue_ynat_eval = load_dataset('klue','ynat',split='validation')
klue_ynat_train[0]
klue_ynat_train.features['label'].names
klue_ynat_train = klue_ynat_train.remove_columns(['guid','url','date'])
klue_ynat_eval = klue_ynat_eval.remove_columns(['guid','url','date'])

def make_str_label(batch):
   batch['label_str'] = klue_ynat_train.features['label'].int2str(batch['label'])
   return batch
  
klue_ynat_train = klue_ynat_train.map(make_str_label, batched=True, batch_size=1000)
klue_ynat_eval = klue_ynat_eval.map(make_str_label, batched=True, batch_size=1000)


train_dataset = klue_ynat_train.train_test_split(test_size=10000, shuffle=True, seed=42)['test']
dataset = klue_ynat_eval.train_test_split(test_size=1000, shuffle=True, seed=42)
test_dataset = dataset['test']
valid_dataset = dataset['train'].train_test_split(test_size=1000, shuffle=True, seed=42)['test']


#dataset = load_dataset("csv", data_files="my_file.csv")
#from datasets import Dataset
#import pandas as pd
#df = pd.DataFrame({"a": [1,2,3]})
#dataset = Dataset.from_pandas(df)


#
# 2. Tokenizer
# tokenizer_config.json
#

import torch
import numpy as np
from transformers import (
Trainer,
TrainingArguments,
AutoModelForSequenceClassification,
AutoTokenizer)

def tokenize_funciton(examples):
   return tokenizer(examples["title"], padding="max_length", truncation=True)

model_id = "klue/roberta-base"
model = AutoModelForSequenceClassification.from_pretrained(model_id, num_labels=len(train_dataset.features['label'].names))
tokenizer = AutoTokenizer.from_pretrained(model_id)

train_dataset = train_dataset.map(tokenize_funciton, batched=True)
valid_dataset = valid_dataset.map(tokenize_funciton, batched=True)
test_dataset = test_dataset.map(tokenize_funciton, batched=True)


#
# 3. Training Model
#

training_args = TrainingArguments (
  output_dir="./results",
  num_train_epochs=1,
  per_device_train_batch_size=8,
  per_device_eval_batch_size=8,
  evaluation_strategy="epoch",
  learning_rate=5e-5,
  push_to_hub=False
)

def compute_metrics(eval_pred):
   logits, labels = eval_pred
   predictions = np.argmax(logits, axis=1)
   return {"accuracy": (predictions == labels).mean()}

trainer = Trainer(
   model=model,
   args=training_args,
   train_dataset = train_dataset,
   eval_dataset = valid_dataset,
   tokenizer = tokenizer,
   compute_metrics = compute_metrics,
)

trainer.train()
trainer.evaluate(test_dataset)

#
# 4. Upload Huggingface
#
from huggingface_hub import login

login(token="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")
repo_id = "jeonseonjin/roberta-base-klue-ynat-classification-1st"

trainer.push_to_hub(repo_id)

#
# 5. Inference

from transformers import pipeline
model_id = "jeonseonjin/results"

model_pipeline  = pipeline("text-classification",model=model_id)
pred = model_pipeline(dataset['test']["title"][:5])
pred


##################################
#
# PEFT : LoRA
#

import torch
from transformers import AutoModelForCausalLM, AutoTokenizer, BitsAndBytesConfig
from peft import LoraConfig, get_peft_model,prepare_model_for_kbit_training

def print_gpu_utilization():
   if torch.cuda.is_available():
      used_memory = torch.cuda.memory_allocated() / 1024**3
      print(f"GPU Memory Usage: {used_memory:.3f} GB")
   else:
      print("Change Run-Time GPU")

      
def load_model_and_tokenizer(model_id, peft=None):
   tokenizer = AutoTokenizer.from_pretrained(model_id)
   
   if peft is None:
      model = AutoModelForCausalLM.from_pretrained(model_id, torch_dtype="auto", device_map={"":0})
   elif peft == 'lora':
      model = AutoModelForCausalLM.from_pretrained(model_id, torch_dtype="auto", device_map={"":0})
      lora_config = LoraConfig(
         r=8,
         local_alpha=32,
         target_modules=["query_key_value"],
         lora_dropout=0.05,
         bias="none",
         task_type="CASUAL_LM"
      )
      model = get_peft_model(model, lora_config)
      model.print_trainable_parameters()
   elif peft == 'qlora': # QLoRA : 4bit 양자화
      model = AutoModelForCausalLM.from_pretrained(model_id, torch_dtype="auto", device_map={"":0})
      lora_config = LoraConfig(
         r=8,
         local_alpha=32,
         target_modules=["query_key_value"],
         lora_dropout=0.05,
         bias="none",
         task_type="CASUAL_LM"
      )
      nf4_config = BitsAndBytesConfig(
         load_in_4bit=True,
         bnb_bit_quant_type="nf4",
         bnb_4bit_use_double_quant=True,
         bnb_4bit_compute_dtype=torch.bfloat16
      )
      model = AutoModelForCausalLM.from_pretrained(model_id, quantization_config=nf4_config, device_map={"":0}))
      model.gradient_checkpoint_enable()
      model = prepare_model_for_kbit_training(model)
      model = get_peft_model(model, lora_config)
      model.print_trainable_parameters()   
   
   print_gpu_utilization()
   return model, tokenizer


